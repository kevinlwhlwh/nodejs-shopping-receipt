# nodejs-shopping-receipt

## Project Information
The project is for input shopping list with certain format and output the summary.

## Pre-requisite
- MacOS/ Linux / Windows OS that able to install Node & NPM (Tested on MacOS) 
- Download and install [Node](https://nodejs.org/en/download/)
- Git Clone the project to your local

## Installation

### Open Your IDE to initialize the project
As soon as you have download nodeJS as above listed at [Pre-requisite](#pre-requisite), open a console (e.g Terminal / iTerm), run the following command line **under project root**
```bash
npm install
```

### Run the application
Input the following to the console
```bash
node index.js
```

### Test with input
To check if the application is worked, input one of the following shopping list
```bash
Location: CA, 1 book at 17.99, 1 potato chips at 3.99
```
```bash
Location: NY, 1 book at 17.99, 3 pencils at 2.99
```
```bash
Location: NY, 2 pencils at 2.99, 1 shirt at 29.99
```

### Input with customized tax rate
To change for the tax rate, run the application with the command
```bash
CA_TAX=0.6 NY_TAX=0.9 node index.js
```

