const stdin = process.stdin;
const taxList = {
    ny: process.env.NY_TAX ? process.env.NY_TAX : 0.08875,
    ca: process.env.CA_TAX ? process.env.CA_TAX : 0.0975
};
const taxExemption = {
    ny: process.env.NY_EXEMPTION ? process.env.NY_EXEMPTION.split(',') : ["potato chips", "candy", "chocolate", "shorts", "shoes", "t-shirt", "shirt"],
    ca: process.env.CA_EXEMPTION ? process.env.CA_EXEMPTION.split(',') : ["potato chips", "candy", "chocolate"],
};


(function () {

    stdin.setEncoding('utf-8');
    console.log("Please input shopping list below");
    stdin.on('data', (data) => {
        const outputData = handleOutputData(data);
        printOutputData(outputData);
        process.exit();
    });

}());

const calculateTax = (item, tax) => {
    const itemName = item[2];
    const itemQty = parseFloat(item[1]);
    const itemPrice = parseFloat(item[3]);
    return taxExemption[tax.location] ? (taxExemption[tax.location].indexOf(itemName) >= 0 ? 0 : (itemPrice * itemQty * tax.tax)) : 0;
};


const taxByLocation = (data) => {
    const location = data.trim().split(',')[0].substring(9).trim().toLowerCase();
    return taxList[location] ?
        {location: location, tax: taxList[location]} :
        {location: location, tax: 0};
};


const handleOutputData = (data) => {
    const output = {
        items: [],
        subTotal: 0,
        tax: 0,
        total: 0
    };
    const tax = taxByLocation(data);
    const items = data.trim().split(',').slice(1).map((item) => {
        const itemDetail = item.trim().match(/^(\d+)\s([a-zA-Z0-9_ ]*)\s\w{2}\s(\d+\.?\d*).*$/);
        output.items.push({
            name: itemDetail[2],
            qty: parseFloat(itemDetail[1]),
            price: parseFloat(itemDetail[3]),
            tax: calculateTax(itemDetail, tax)
        })
    });

    output.subTotal = roundUp(
        parseFloat(output.items.reduce(((previousValue, {price, qty}) => {
            return previousValue + price * qty;
        }), 0)), 0.001);


    output.tax = roundUpTax(parseFloat(output.items.reduce((previousValue, {tax}) => {
        return previousValue + tax
    }, 0)));

    output.total = roundUp((output.subTotal + output.tax), 0.01);

    return output;
};


const printOutputData = (data) => {
    console.log("item".padStart(30) + "price".padStart(30) + "qty".padStart(30) + "\n");
    data.items.map((item) => {
        console.log(item.name.padStart(30) + item.price.toString().padStart(30) + item.qty.toString().padStart(30))
    });
    console.log("subtotal".padStart(30) + data.subTotal.toString().padStart(60));
    console.log("tax".padStart(30) + data.tax.toFixed(2).padStart(60));
    console.log("total".padStart(30) + data.total.toString().padStart(60));
};


const roundUp = (value, step) => {
    let roundUpNumber = 0;
    const inv = 1.0 / (step || 1.0);
    return (Math.round(value * inv) / inv) + roundUpNumber;
};

const roundUpTax = (value) => {
    const remainer = value % 0.05;
    return (value % 0.05 > 0) ? (value - remainer + 0.05) : value;
};
